﻿using UnityEngine;
using System.Collections;

public class LoadLevelEnter : MonoBehaviour {

    public string loadLevel = "Tutorial";

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("ColisionEnter");
        if(collider.tag == "Player")
        {
            Debug.Log("successfull entered");
            Application.LoadLevel(loadLevel);
        }
    }
}
