﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;

public class DamageHandler : MonoBehaviour {

    [SerializeField]
    float startHealth = 100f;

    float currentHealth;
    [SerializeField]
    float damageThreshold = 2f;

    Vector3 respawnPoint;

    ScreenOverlay m_ScreenOverlay;
    ColorAdjustments m_ColorAdjustments;

	// Use this for initialization
	void Start () {
        // respawn at start.
        respawnPoint = transform.position;
        currentHealth = startHealth;
        m_ScreenOverlay = FindObjectOfType<ScreenOverlay>();
        m_ColorAdjustments = FindObjectOfType<ColorAdjustments>();
	}

    void Respawn()
    {
        // reset position to last known spawnpoint
        transform.position = respawnPoint;
        // reset health
        currentHealth = startHealth;
    }

    // Update is called once per frame
    void Update () 
    {
        if (Input.GetButtonDown("Respawn"))
            Respawn();

        if (Input.GetButtonDown("ResetLevel"))
        {
            Application.LoadLevel(Application.loadedLevelName);
        }

	    if(currentHealth <= 0)
        {
            // Display death screen
            Debug.Log("YOU ARE DEAD!");

            Respawn();
        }

        m_ScreenOverlay.intensity -= 0.05f;

        if (m_ScreenOverlay.intensity < 0)
            m_ScreenOverlay.intensity = 0;

        m_ColorAdjustments.brightnessAmount += 0.05f;

        if (m_ColorAdjustments.brightnessAmount > 1)
            m_ColorAdjustments.brightnessAmount = 1;

        m_ColorAdjustments.saturationAmount = currentHealth / startHealth;
    }

    void OnTriggerEnter(Collider collider)
    {
        // set respawn on triggers too.
        if (collider.isTrigger && collider.tag == "Respawn")
        {
            //anything that is tagged as respawn will act as a spawn point
            respawnPoint = this.transform.position;

            Debug.Log("New Spawn point set: " + respawnPoint);
        }

        if (collider.tag == "DeathZone")
        {
            Debug.Log("you touched a deathzone... diieee!");
            currentHealth = 0.0f;
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        // set respawn is respawn tag
        if (collision.collider.tag == "respawn")
        {
            //anything that is tagged as respawn will act as a spawn point
            respawnPoint = this.transform.position;

            Debug.Log("New Spawn point set: " + respawnPoint);
        }
        
        if(collision.collider.tag == "DeathZone")
        {
            Debug.Log("YOU TOUCHED A DEATHZONE... DIIEEE!");
            currentHealth = 0.0f;
        }

        // damage if hit with a lot of force
        float collisionForce = collision.relativeVelocity.magnitude;
        if(collisionForce > damageThreshold && currentHealth > 0)
        {
            currentHealth -= collisionForce;
            float fac = (startHealth - currentHealth) / startHealth;
            m_ScreenOverlay.intensity = fac;
            m_ColorAdjustments.brightnessAmount = 1 - fac;
            // TODO: Notify here
            Debug.Log("OUCHIES! Took " + collisionForce + " Damage, Health remaining: " + currentHealth);
        }
    }
}
