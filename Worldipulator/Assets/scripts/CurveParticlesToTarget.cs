﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class CurveParticlesToTarget : MonoBehaviour {

    [SerializeField]
    public Vector3 m_targetPosition;

    private ParticleSystem m_thisParticleSystem;
    private ParticleSystem.Particle[] m_allTheParticles;

	// Use this for initialization
	void Start () 
    {
        if(m_thisParticleSystem == null)
            m_thisParticleSystem = GetComponent<ParticleSystem>();

        if (m_allTheParticles == null || m_allTheParticles.Length < m_thisParticleSystem.maxParticles)
            m_allTheParticles = new ParticleSystem.Particle[m_thisParticleSystem.maxParticles];
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(m_targetPosition.sqrMagnitude > 0)
        {
            int numParticlesAlive = m_thisParticleSystem.GetParticles(m_allTheParticles);
            Vector3 positionMid = Camera.main.transform.forward * Vector3.Distance(m_targetPosition, transform.position) * 0.5f;
            for (int i = 0; i < numParticlesAlive; i++)
            {
                float currentStep = Mathf.SmoothStep(0f, 1f, m_allTheParticles[i].remainingLifetime / m_allTheParticles[i].startLifetime);
                Vector3 startToMid = transform.position * currentStep + positionMid * (1f - currentStep);
                Vector3 MidToEnd = positionMid * currentStep + m_targetPosition * (1f - currentStep);

                m_allTheParticles[i].position = startToMid * currentStep + MidToEnd * (1f - currentStep); // +positionMid * (1f - Mathf.Abs(secondStep));
            }
            m_thisParticleSystem.SetParticles(m_allTheParticles, numParticlesAlive);
        }
        
	}
}
