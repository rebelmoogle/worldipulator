﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

    CursorLockMode cursorLocked;

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    bool paused = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            paused = togglePause();
    }

    void OnGUI()
    {
        if (paused)
        {
            //Cursor.lockState = CursorLockMode.None;
            GUILayout.BeginArea(new Rect(Screen.width/2 - 100, Screen.height/2 - 50, 200, 100));
            GUILayout.Label("Game is paused!");
            if (GUILayout.Button("Click me to Quit"))
            {
                Application.Quit();
            }
            if (GUILayout.Button("Click me to Menu"))
            {
                Application.LoadLevel("Menu");
            }
            if (GUILayout.Button("Click me to Restart Level"))
            {
                paused = togglePause();
                Application.LoadLevel(Application.loadedLevelName);
            }
            GUILayout.EndArea();
        }
        //Cursor.lockState = CursorLockMode.Locked;
    }

    bool togglePause()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            return (false);
        }
        else
        {
            Time.timeScale = 0f;
            return (true);
        }
    }
}
