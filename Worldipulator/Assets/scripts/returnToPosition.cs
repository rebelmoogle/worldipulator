﻿using UnityEngine;
using System.Collections;

public class returnToPosition : MonoBehaviour {

    [SerializeField]
    private Vector3 startPosition;
    private Rigidbody myBody;

    [SerializeField]
    private float returningForce = 10.0f;

	// Use this for initialization
	void Start ()
    {
        myBody = this.GetComponent<Rigidbody>();
        if(myBody)
        {
            myBody.useGravity = false;
        }

        if(startPosition.sqrMagnitude == 0)
            startPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(myBody)
        {
            Vector3 direction = startPosition - transform.position;

            myBody.AddForce(direction * Time.deltaTime * returningForce, ForceMode.Impulse);
        }
        
	}

}
