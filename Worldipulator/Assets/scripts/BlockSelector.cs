﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public sealed class BlockSelector : MonoBehaviour
{
	// Use this for initialization
	void Start () {
        GetComponent<DragRigidbody>().SelectionEvent += Select;
        lightGameObject = new GameObject();
        lightComp = lightGameObject.AddComponent<Light>();
        lightComp.color = Color.cyan;
        lightComp.intensity = 0;
        lightComp.range = 1;
        curve = m_RayParticleSystem.GetComponent<CurveParticlesToTarget>();
    }

    private GameObject lastObject;
    private GameObject lightGameObject;
    private Light lightComp;
    private bool isSelected;
    private CurveParticlesToTarget curve;

    public ParticleSystem m_ParticleSystem;

    public ParticleSystem m_RayParticleSystem;

    private void Select(object sender, SelectionEventArgs args)
    {
        if ((args.Hit != null && args.Hit.Value.rigidbody && !args.Hit.Value.rigidbody.isKinematic) || isSelected)
        {
            if(!m_ParticleSystem)
            {
                Debug.Log("NO PARTICLE SYSTEM in BlockSelector, uitting select method");
                return;
            }

            if (!m_ParticleSystem.isPlaying)
                m_ParticleSystem.Play();

            if (!m_RayParticleSystem.isPlaying)
                m_RayParticleSystem.Play();

            lightComp.enabled = true;
            lightComp.intensity += 0.2f;

            if (lightComp.intensity >= 2)
                lightComp.intensity = 2;

            if (args.Hit == null || isSelected)
            {
                Vector3 pos = lastObject.GetComponent<Rigidbody>().transform.position - transform.position;
                pos.Normalize();
                SetLight(lastObject.GetComponent<Rigidbody>().transform.position - pos);
            }
            else
            {
                Vector3 pos = args.Hit.Value.point - transform.position;
                pos.Normalize();
                SetLight(args.Hit.Value.point - pos);
                m_ParticleSystem.transform.LookAt(m_ParticleSystem.transform.position + args.Hit.Value.normal);
            }
        }
        else
        {
            lightComp.intensity -= 0.2f;

            if (lightComp.intensity <= 0)
            {
                lightComp.intensity = 0;
                lightGameObject.GetComponent<Light>().enabled = false;
                if (m_ParticleSystem)
                {
                    m_ParticleSystem.Stop();
                }
                
                m_RayParticleSystem.Stop();
                m_RayParticleSystem.Clear();
            }
        }

        BlockHighlighter blkhigh = null;

        if (isSelected)
        {
            if (args.StopFiring)
            {
                lastObject.GetComponent<BlockHighlighter>().IsSelected = false;
                isSelected = false;
            }
            else
                return;
        }

        if (args.Hit == null || !args.Hit.Value.rigidbody || args.Hit.Value.rigidbody.isKinematic)
        {
            if (lastObject != null)
            {
                lastObject.GetComponent<BlockHighlighter>().IsHighlighted = false;
                lastObject = null;
            }
        }
        else if (lastObject == null)
        {
            blkhigh = args.Hit.Value.rigidbody.gameObject.GetComponent<BlockHighlighter>();
            if (blkhigh == null)
                blkhigh = args.Hit.Value.rigidbody.gameObject.AddComponent<BlockHighlighter>();

            blkhigh.IsHighlighted = true;

            lastObject = args.Hit.Value.rigidbody.gameObject;
        }
        else if (args.Hit.Value.rigidbody.gameObject != lastObject)
        {
            lastObject.GetComponent<BlockHighlighter>().IsHighlighted = false;

            blkhigh = args.Hit.Value.rigidbody.gameObject.GetComponent<BlockHighlighter>();
            if (blkhigh == null)
                blkhigh = args.Hit.Value.rigidbody.gameObject.AddComponent<BlockHighlighter>();

            blkhigh.IsHighlighted = true;
            lastObject = args.Hit.Value.rigidbody.gameObject;
        }

        if (lastObject == null)
            return;

        if (blkhigh == null)
            blkhigh = lastObject.GetComponent<BlockHighlighter>();

        if (args.StartFiring)
        {
            blkhigh.IsSelected = true;
            isSelected = true;
        }
    }

    private void SetLight (Vector3 pos)
    {
        lightGameObject.transform.position = pos;
        m_ParticleSystem.transform.position = pos;
        curve.m_targetPosition = pos;
    }

    // Update is called once per frame
    void Update () {
	}
}
