﻿using UnityEngine;
using System.Collections;

public class BlockHighlighter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        m_Renderer = GetComponent<Renderer>();
    }

    private Renderer m_Renderer;

    private Color m_CachedHightlightColor;

    private Color m_CachedColor;

    private bool m_IsHighlighted;
    
    private bool m_DoFade;

    private float fac;
    public bool IsHighlighted {
        get
        {
            return m_IsHighlighted;
        }
        set
        {
            if (!m_IsHighlighted && !m_DoFade)
            {
                m_CachedColor = GetComponent<Renderer>().material.color;
                m_CachedHightlightColor = m_CachedColor + new Color(0.5f, 0.5f, 0.5f, 1);
                m_IsHighlighted = value;
                m_DoFade = true;
            }
            else
            {
                m_IsHighlighted = value;
                m_DoFade = true;
            }
        }
    }

    public bool IsSelected { get; set; }

    // Update is called once per frame
    void Update()
    {
        if (IsSelected)
            return;

        if (m_DoFade)
        {
            if (IsHighlighted)
            {
                fac += 0.1f;

                if (fac > 1)
                {
                    fac = 1;
                    m_DoFade = false;
                }
            }
            else
            {
                fac -= 0.1f;

                if (fac < 0)
                {
                    fac = 0;
                    m_DoFade = false;
                }
            }
        }

        m_Renderer.material.color = m_CachedColor * (1 - fac) + m_CachedHightlightColor * fac;
     
        if (!m_DoFade && !m_IsHighlighted)
            Destroy(this);
    }
}
