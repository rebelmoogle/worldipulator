﻿using UnityEngine;
using System.Collections;

public class DirectionalForce : MonoBehaviour {

    public float ForceLength;

    public float ForceStrength;

	// Use this for initialization
	void Start () {
	
	}
	
	void FixedUpdate () {
        RaycastHit[] colliders = Physics.SphereCastAll(transform.position, 1, transform.up, ForceLength);

        foreach (RaycastHit hit in colliders)
        {
            if (hit.rigidbody != null && !(hit.rigidbody.gameObject == gameObject))
                hit.rigidbody.AddForce(transform.up * ForceStrength * Time.fixedDeltaTime, ForceMode.Impulse);
        }
	}
}
