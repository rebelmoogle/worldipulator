﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
    public void ChangeToScene(string levelName)
    {
        Debug.Log("Change Level");
        
        Application.LoadLevel(levelName);
        
    }

	// Update is called once per frame
	void Update () {
	}
}
