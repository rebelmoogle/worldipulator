﻿using UnityEngine;
using System.Collections;

public class ShowText : MonoBehaviour {

    public string shownText;
    public Color textColor = Color.black;
    bool hitPlayer = false;
    GUIStyle style = new GUIStyle();
    int maxLength = 25;

	// Use this for initialization
	void Start () {
        style.fontSize = 30;
        if (shownText.Length > maxLength)
        {
            string[] words = shownText.Split(' ');
            shownText = "";
            string oneLine = "";
            for(int i = 0; i<words.Length; ++i)
            {
                if (oneLine.Length < maxLength)
                    oneLine += words[i] + " ";
                else
                {
                    shownText += oneLine + "\n";
                    oneLine = words[i] + " ";
                }
            }
            shownText += oneLine;
        }
        style.normal.textColor = textColor;
    }
	
	// Update is called once per frame
	void Update () {
       
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "Player")
            hitPlayer = true;
    }

    void OnTriggerExit(Collider c)
    {
        if (c.gameObject.tag == "Player")
            hitPlayer = false;
    }

    void OnGUI()
    {
        if (hitPlayer)
        {
            GUI.color = textColor;
            GUI.Label(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 400, 200), shownText, style);
        }
    }
}
