using System;
using System.Collections;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public sealed class SelectionEventArgs
    {
        public SelectionEventArgs(RaycastHit? hit, bool startFiring, bool stopFiring)
        {
            Hit = hit;
            StartFiring = startFiring;
            StopFiring = stopFiring;
        }
        
        public RaycastHit? Hit { get; private set; }

        public bool StartFiring { get; private set; }

        public bool StopFiring { get; private set; }
        
    }

    public class DragRigidbody : MonoBehaviour
    {

        const float k_Spring = 50.0f;
        const float k_Damper = 5.0f;
        const float k_Drag = 10.0f;
        const float k_AngularDrag = 5.0f;
        const float k_Distance = 0.2f;
        const bool k_AttachToCenterOfMass = false;

        private SpringJoint m_SpringJoint;

        [SerializeField]
        private float magicMoveForce = 20f;

        [SerializeField]
        private float maximumGrabDistance = 10.0f;
        

        public delegate void SelectionEventHandler(object sender, SelectionEventArgs e);
        public event SelectionEventHandler SelectionEvent;

        private void FixedUpdate()
        {
            var mainCamera = FindCamera();

            bool startFiring = Input.GetButtonDown("Fire1");
            bool stopFiring = Input.GetButtonUp("Fire1") || !Input.GetButton("Fire1");

            // We need to actually hit an object
            RaycastHit hit = new RaycastHit();
            if (
                !Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin,
                                 mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, maximumGrabDistance,
                                 Physics.DefaultRaycastLayers))
            {
                if (SelectionEvent != null)
                    SelectionEvent(this, new SelectionEventArgs(null, startFiring, stopFiring));
                return;
            }

            if (SelectionEvent != null)
                SelectionEvent(this, new SelectionEventArgs(hit, startFiring, stopFiring));

            // We need to hit a rigidbody that is not kinematic
            if (!hit.rigidbody || hit.rigidbody.isKinematic)
            {
                return;
            }

            // Make sure the user pressed the mouse down
            if (!startFiring)
            {
                return;
            }

            if (!m_SpringJoint)
            {
                var go = new GameObject("Rigidbody dragger");
                Rigidbody body = go.AddComponent<Rigidbody>();
                m_SpringJoint = go.AddComponent<SpringJoint>();
                body.isKinematic = true;
            }

            m_SpringJoint.transform.position = hit.point;

            m_SpringJoint.anchor = Vector3.zero;

            m_SpringJoint.spring = k_Spring;
            m_SpringJoint.damper = k_Damper;
            m_SpringJoint.maxDistance = k_Distance;
            m_SpringJoint.connectedBody = hit.rigidbody;

            StartCoroutine("DragObject", hit.distance);
        }


        private IEnumerator DragObject(float distance)
        {
            var oldDrag = m_SpringJoint.connectedBody.drag;
            var oldAngularDrag = m_SpringJoint.connectedBody.angularDrag;
            m_SpringJoint.connectedBody.drag = k_Drag;
            m_SpringJoint.connectedBody.angularDrag = k_AngularDrag;
            var mainCamera = FindCamera();
            
            while (Input.GetMouseButton(0))
            {
                var ray = mainCamera.ScreenPointToRay(Input.mousePosition);

                Vector3 rayPoint = ray.GetPoint(distance);

                    if (Mathf.Abs(Vector3.Dot(new Vector3(mainCamera.transform.forward.x, 0, mainCamera.transform.forward.z), Vector3.forward)) > 0.5f)
                    {
                        //looking forward or backward
                        rayPoint = new Vector3(rayPoint.x, rayPoint.y, m_SpringJoint.transform.position.z + Input.GetAxis("ScrollWheel") * magicMoveForce);
                    }
                    else
                    {
                        // looking left or right
                        rayPoint = new Vector3(m_SpringJoint.transform.position.x + Input.GetAxis("ScrollWheel") * magicMoveForce, rayPoint.y, rayPoint.z);
                    }
                             

                m_SpringJoint.connectedBody.constraints = RigidbodyConstraints.FreezeRotation;
 
                m_SpringJoint.transform.position = rayPoint;
                yield return null;
            }
            if (m_SpringJoint.connectedBody)
            {
                m_SpringJoint.connectedBody.drag = oldDrag;
                m_SpringJoint.connectedBody.angularDrag = oldAngularDrag;
                m_SpringJoint.connectedBody = null;
            }
        }


        private Camera FindCamera()
        {
            if (GetComponent<Camera>())
            {
                return GetComponent<Camera>();
            }

            return Camera.main;
        }

    }
}
