﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour {

    public Texture2D crosshairImage;
    public Rect position;

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update () {
        
    }

    void OnGUI()
    {
        //centerScene
        float xMin = (Screen.width / 2) - (crosshairImage.width / 2);
        float yMin = (Screen.height / 2) - (crosshairImage.height / 2);

        //mousePosition
        //float xMin = Screen.width - (Screen.width - Input.mousePosition.x) - (crosshairImage.width / 2);
        //float yMin = (Screen.height - Input.mousePosition.y) - (crosshairImage.height / 2);

        position = new Rect(xMin, yMin, crosshairImage.width, crosshairImage.height);
        GUI.DrawTexture(position, crosshairImage);
    }
}
