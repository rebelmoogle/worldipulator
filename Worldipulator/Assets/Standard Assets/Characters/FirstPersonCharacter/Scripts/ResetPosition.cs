﻿using UnityEngine;
using System.Collections;

public class ResetPosition : MonoBehaviour {

    Vector3 startposition;

    // Use this for initialization
    void Start () {
        startposition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	    if(gameObject.transform.position.y <= -100)
        {
            gameObject.transform.position = startposition;
        }
	}
}
